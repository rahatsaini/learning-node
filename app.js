console.log('Starting app.js');

const fs = require('fs');
const _ = require('lodash');
const yargs = require('yargs');


const notes = require('./notes.js');

const titleOptions = {
  describe:'Title of note',
  demand:true,
  alias :'t'
  };

const bodyOptions ={
  describe:'body of the note',
  demand:true
  ,alias:'b'
};


const argv = yargs.command('add',"add a new note",{
  title : titleOptions,
  body :bodyOptions
}).command('list','lists all the notes')
.help()
.argv;

var command = argv._[0];
console.log('Command: ', command);
console.log('Yargs', argv);


//add command

if (command === 'add') {
  var note = notes.addNote(argv.title, argv.body);
  if (note) {
    console.log('Note created');
    console.log('--');
    console.log(`Title: ${note.title}`);
    console.log(`Body: ${note.body}`);
  } else {
    console.log('Note title taken');
  }
} 


else if (command === 'list') {
  
  var allNotes = notes.getAll();
  console.log(`Printing ${allNotes.length} note(s).` );
  allNotes.forEach((note) => { 
     console.log(note);
  });

} else if (command === 'read') {
  
  var note = notes.getNote(argv.title);
  console.log(note);
  if(note)
  console.log("note found : " + note.body);
  else 
    console.log("note not found");
} else if (command === 'remove') {
  var noteRemoved = notes.removeNote(argv.title);
  var message = noteRemoved ? "Note Removed" : "No note found";
  console.log(message);
} else {
  console.log('Command not recognized');
}
